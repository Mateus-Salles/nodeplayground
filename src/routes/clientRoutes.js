const express = require('express');
const router = express.Router();

// Importando o controlador de clientes
const userController = require('../controller/clientController');

// Definindo as rotas para os clientes
router.get('/', userController.getAllClients);
router.post('/', userController.createClient);
router.put('/:id', userController.updateClient);
router.delete('/:id', userController.deleteClient);

module.exports = router;