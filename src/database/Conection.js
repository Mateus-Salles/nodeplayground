require('dotenv').config();
const postgres = require('postgres');

const { PGUSER, PGPASSWORD, PGHOST, PGDATABASE } = process.env;

const URL = `postgresql://${PGUSER}:${PGPASSWORD}@${PGHOST}/${PGDATABASE}`;

const sql = postgres(URL, { ssl: 'require' });

module.exports = sql;