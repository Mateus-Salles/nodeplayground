const DataBase = require('../database/Database');

const dataBase = new DataBase;

// (GET) Mostrando todos os clientes
const getAllClients = async (request, response) => {
    const clientes = await dataBase.show();

    return response.status(200).send(clientes);
}

// (POST) Cadastrando um novo cliente
const createClient = async (request, response) => {
    const requestBody = request.body;

    await dataBase.create(requestBody);

    return response.status(201).send();
}

// (PUT) Modificando as informações de um cliente
const updateClient = async (request, response) => {
    const clientId = request.params.id;

    const clientNewInfo = request.body;

    await dataBase.update(clientId, clientNewInfo);

    return response.status(204).send();
}

// (DELETE) Deletando um cliente
const deleteClient = async (request, response) => {
    const clientId = request.params.id;

    await dataBase.delete(clientId);

    return response.status(204).send();
}

// Exportando todos os módulos
module.exports = {
    getAllClients,
    createClient,
    updateClient,
    deleteClient
}