const sql = require('./Conection');

class DataBase {
    async show() {
        return await sql`SELECT * FROM clientes`;
    }

    async create({ name, birthDate, sex }) {
        return await sql`INSERT INTO clientes(nome, data_nasc, sexo) VALUES (${name}, ${birthDate}, ${sex})`;
    }

    async update(id, { name, birthDate, sex }) {
        return await sql`UPDATE clientes SET nome=${name}, data_nasc=${birthDate}, sexo=${sex} WHERE id=${id}`;
    }

    async delete(id) {
        return await sql`DELETE FROM clientes WHERE id=${id}`;
    }
}

module.exports = DataBase;