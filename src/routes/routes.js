const express = require('express');
const clientRoutes = require('./clientRoutes');

const router = express.Router();

// Unindo todas as rotas
router.use('/clientes', clientRoutes);

// Exportando todas as rotas
module.exports = router;