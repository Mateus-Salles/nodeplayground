const express = require('express');
const routes = require('./routes/routes');

const app = express();

app.use(express.json());

// Configura o prefixo '/api' para usar as rotas
app.use('/api', routes);


app.get('/', (request, response) => {
    return response.status(200).send('Hello World');
});

app.listen(3333, () => {
    console.log('Server running on Port 3333');
});